#!/bin/sh

DIRECTORY=${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k


if [ ! -d "$DIRECTORY" ]; then
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $DIRECTORY
fi
