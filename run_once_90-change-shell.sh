#!/bin/sh

set -eufo pipefail

# Change Shell to ZSH
chsh -s $(which zsh)
