#!/bin/sh

# Docker install
sudo pacman -Syu docker

sudo systemctl start docker.service
sudo systemctl enable docker.service

sudo usermod -aG docker $USER

# Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
