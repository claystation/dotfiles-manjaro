call plug#begin('~/.config/nvim/plugged')

Plug 'tpope/vim-surround'
Plug 'bling/vim-airline'
Plug 'arcticicestudio/nord-vim'

call plug#end()

set number
set relativenumber

set expandtab shiftwidth=2 softtabstop=2 tabstop=4 textwidth=80
set modeline

if has('termguicolors')
  set termguicolors
endif

colorscheme nord
